package com.example.selectionSort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = SelectionSort.createRandomArray(20, -2, 8);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        SelectionSort.selectionSort(array);
        System.out.println(Arrays.toString(array));
    }
}
