package com.example.selectionSort;

import java.util.Random;

class SelectionSort {
    /**
     * złożoność obliczeniowa: O(n^2)
     * złożoność pamięciowa: Omega(1)
     */
    static void selectionSort(int[] array) {
        //i = origin of unsorted-sub-array
        for (int i = 0; i < array.length; i++) {
            int tempMinIndex = i;
            //find minimum value in unsorted sub-array
            for (int j = i; j < array.length; j++) {
                if (array[tempMinIndex] > array[j]) {
                    tempMinIndex = j;
                }
            }
            //swap first value from unsorted sub-array with found minimum
            int temp = array[i];
            array[i] = array[tempMinIndex];
            array[tempMinIndex] = temp;
        }
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}

